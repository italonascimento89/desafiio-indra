# ProjetoIndra

Este projeto foi gerado com[Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Servidor de desenvolvimento

Execute `ng serve` para um servidor dev. Navegue até `http: // localhost: 4200 /`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

Para simular o servidor rest, execuete o comando json-server db.json

## Primeiro passo; efetuar um fork do projeto

Efetuar uma copia do repositorio do projeto clicando no button na lateral direita "CLONE"

## Segundo passo; efetuar um fork do projeto 

Escolha o tipo de protocolo no caso HTTPS ou SSH caso vc tenha permissão, recomendo HTTPS, que fica no menur CLONE no topo
superior direito.

Apos copiar este url, var na sua IDE abra o terminal dela, certifique-se que você esta dentro do seu repositorio que você criou para organizar seu projeto ($ projetos/www/html/) e digite;

$ git clonne git@gitlab.com:italonascimento89/desafiio-indra.git

Após clonar o projeto, com o terminal aberto execute o comando NPM I, para baixar as dependências do projeto

## Tecnologias

* HTML
* CSS
* JAVASCRIPT
* TYPESCRIPT
* ANGULAR 8.2
* ANGULAR CLI 8.2.2
* JSON-SERVER
* NGX-BOOSTRAP

