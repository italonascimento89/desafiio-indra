import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './partls/navbar/navbar.component';
import { SidebarComponent } from './partls/sidebar/sidebar.component';
import { FooterComponent } from './partls/footer/footer.component';

import { UsuariosModule } from './usuarios/module/usuarios.module';
import { HistoricoCombustivelModule } from './historico-combustivel/module/historico-combustivel.module';
import { CadastroUsuariosModule } from './cadastro-usuarios/module/cadastro-usuarios.module';
import { NotfoundComponent } from './notfound/notfound.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    NotfoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UsuariosModule,
    HistoricoCombustivelModule,
    CadastroUsuariosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
