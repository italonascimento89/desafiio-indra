import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

import { UsuariosInterface } from '../../usuarios/model/usuarios.interface';

@Injectable({
  providedIn: 'root'
})
export class CadastroUsuariosService {

  private readonly API = `${environment.API}/usuarios` ;

  constructor(private http: HttpClient) { }


  public loadById(id) {
    return this.http.get<UsuariosInterface>(
      `${this.API}/${id}`).pipe(
        take(1)
      );
  }

  public create(usuario) {
    return this.http.post(this.API, usuario)
           .pipe(
             take(1)
           );
  }

  public update(usuario) {
     return this.http.put(`${this.API}/${usuario.id}`, usuario)
      .pipe(
        take(1)
      );
  }

  public save(usuario) {

    if(usuario.id){
       return this.update(usuario);
    }
    else {
       return this.create(usuario);
    }

  }



}
