import { TestBed } from '@angular/core/testing';

import { CadastroUsuariosService } from './cadastro-usuarios.service';

describe('CadastroUsuariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastroUsuariosService = TestBed.get(CadastroUsuariosService);
    expect(service).toBeTruthy();
  });
});
