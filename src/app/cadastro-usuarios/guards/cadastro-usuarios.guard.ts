
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';

import { UsuariosInterface } from '../../usuarios/model/usuarios.interface';
import { CadastroUsuariosService } from '../service/cadastro-usuarios.service';

@Injectable({
  providedIn: 'root'
})
export class CursoResolverGuard implements Resolve<UsuariosInterface> {
  constructor(private service: CadastroUsuariosService) {}

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UsuariosInterface> {
    if (route.params && route.params['id']) {
       return this.service.loadById(route.params['id']);
    }

    return of({
      id: null,
      nome: null,
      email: null,
      usuario: null,
      senha: null
    });
  }
}