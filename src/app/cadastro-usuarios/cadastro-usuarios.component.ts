import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, ValidationErrors } from '@angular/forms';

import { CadastroUsuariosService } from './service/cadastro-usuarios.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlterModalService } from '../shared/service/alter-modal.service';
import { BaseFormComponent } from '../shared/base-form/base-form.component';
import { SidebarService } from '../partls/sidebar/service/sidebar.service';

@Component({
  selector: 'app-cadastro-usuarios',
  templateUrl: './cadastro-usuarios.component.html',
  styleUrls: ['./cadastro-usuarios.component.css']
})
export class CadastroUsuariosComponent  extends BaseFormComponent implements OnInit {

  public headerPageActive: boolean;

  constructor(
   private fb: FormBuilder,
   public modal: AlterModalService,
   public service: CadastroUsuariosService,
   private route: ActivatedRoute,
   private router: Router,
   private sidebarService: SidebarService 
  ) {

    super(
      service,
      modal
    );
   
  }
  
  ngOnInit() {

  const usuario = this.route.snapshot.data['usuario'];

   this.form = this.fb.group({
      id:[usuario.id],
      nome:[
        usuario.nome, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ]
    ],
      email:[
        usuario.email, [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i),
        Validators.maxLength(100) 
        ]
    ],
      usuario: [
        usuario.usuario, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(12) 
        ]
      ],
      senha: [
        usuario.senha, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12) 
        ]
      ]
      
    });

    this.headerActive();

  }

  public submit() {
    this.submiited = true;
    console.log(this.form.value);
    if (this.form.valid) {
      console.log('submit');

      let msgSuccess = 'Usuario Cadastrado com sucesso';
      let msgError = 'Erro ao cadastrar usuario, tente novamente';

      if (this.form.value.id) {
        msgSuccess = 'Usuario atualizado com sucesso!';
        msgError = 'Erro ao atualizar usuario, tente novamente!';
      }

       if (this.form.value.id) {
        this.service.update(this.form.value).subscribe(
          success => {
            this.modal.showAlertSucess(msgSuccess);
          },
          error => this.modal.showAlertDanger(msgError),
          () => console.log('update completo')
        );
      } else {
        this.service.create(this.form.value).subscribe(
          success => {
            this.modal.showAlertSucess(msgSuccess);
          },
          error => this.modal.showAlertDanger(msgError),
          () => console.log('request completo')
        );
      }
    } 
  }

  public hasError(field: string): ValidationErrors {
    return this.form.get(field).errors;
  }

  public onCancel(): void {
    this.submiited = false;
    this.form.reset();
    this.routerCancel();
  }
  
  public routerCancel(): void {

    this.router.navigateByUrl('/usuarios');
    
  }

  
  public headerActive(): boolean {

    return this.headerPageActive = this.sidebarService.getHeaderActive();

  }
  

}
