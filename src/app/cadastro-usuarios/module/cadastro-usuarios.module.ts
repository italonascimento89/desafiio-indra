import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CadastroUsuariosRoutingMudule } from './cadastro-usuarios-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { CadastroUsuariosComponent } from '../cadastro-usuarios.component';

@NgModule({
  declarations: [
    CadastroUsuariosComponent,
  ],
  imports: [
    CommonModule,
    CadastroUsuariosRoutingMudule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CadastroUsuariosModule { }
