import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursoResolverGuard } from '../guards/cadastro-usuarios.guard';
import { CadastroUsuariosComponent } from '../cadastro-usuarios.component';

const routes: Routes = [
  {
    path: '',
    component: CadastroUsuariosComponent,
    resolve: {
      usuario: CursoResolverGuard
    },
    data: { title: 'Cadastro Usuarios' }
  },
  {
    path: 'editar-usuarios/:id',
    component: CadastroUsuariosComponent,
    resolve: {
      usuario: CursoResolverGuard
    },
    data: { title: 'Editar usuários' }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadastroUsuariosRoutingMudule { }