import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosComponent } from './usuarios/usuarios.component';
import { NotfoundComponent } from './notfound/notfound.component';

const APP_ROUTES: Routes = [
    { path: '', component: UsuariosComponent },
    {
      path: 'usuarios',
      loadChildren: ()=> import('./usuarios/module/usuarios.module').then(m => m.UsuariosModule),
      data: { title: 'Listar Usuarios' }
    },
    {
      path: 'historico-combustivel',
      loadChildren: () => import('./historico-combustivel/module/historico-combustivel.module').then(m => m.HistoricoCombustivelModule),
      data: { title: 'Historico Combustivel' }
    },
    {
      path: 'cadastro-usuarios',
      loadChildren: () => import('./cadastro-usuarios/module/cadastro-usuarios.module').then(m => m.CadastroUsuariosModule),
      data: { title: 'Cadastro de usuarios' }
    },
    { path: '', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



