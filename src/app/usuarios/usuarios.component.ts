import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, empty, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UsuariosService } from './service/usuarios-service.service';
import { UsuariosInterface } from './model/usuarios.interface';
import { AlterModalService } from '../shared/service/alter-modal.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SidebarService } from '../partls/sidebar/service/sidebar.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  public usuarios$: Observable<UsuariosInterface[]>;
  public error$ = new Subject<boolean>();
  public deleteModalRef: BsModalRef;
  public usuarioSelecionado: UsuariosInterface;
  @ViewChild('deleteModal', { static: true }) deleteModal;

  constructor(
    private service: UsuariosService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private alertService: AlterModalService,
    private sidebarService: SidebarService 
  ) { }

  ngOnInit() {
    this.listarUsuarios();
  }

  private listarUsuarios(): void {

    this.usuarios$ = this.service.listarUsuarios()
    .pipe(
      catchError( error => {
        console.error(error)
        this.handleError();
        return empty();
      })
    );

  }

  public onEdit(id): void {

    this.router.navigate(
      ['editar-usuarios', id]
    );

  }

  public handleError(): void {

    this.alertService
    .showAlertDanger('Erro ao carregar usuarios. Tente novamente mais tarde.');
    
  }

  public onDelete(usuario): void {

    this.usuarioSelecionado = usuario;
    this.deleteModalRef = this.modalService.show(this.deleteModal, { class: 'modal-sm' });

  }

  public onConfirmDelete() {

    this.service.remove(this.usuarioSelecionado.id)
    .subscribe(
      success => {
        this.listarUsuarios();
        this.deleteModalRef.hide();
      },
      error => {
        this.alertService.showAlertDanger('Erro ao remover curso. Tente novamente mais tarde.');
        this.deleteModalRef.hide();
      }

    );
  }

  public onDeclineDelete() {

    this.deleteModalRef.hide();

  }

  
  public setHeaderActive() {

    this.sidebarService.setHeaderActive('editar');
    
  }
  

}
