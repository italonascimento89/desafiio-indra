import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { mergeMap, delay } from 'rxjs/internal/operators';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { UsuariosInterface } from '../model/usuarios.interface';


@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private readonly API = `${environment.API}/usuarios` ;

  constructor(
    private http: HttpClient
  ) { }

  public listarUsuarios(): Observable<UsuariosInterface[]> {
    return this.http.get<any>(this.API)
    .pipe(
          delay(2000),
          mergeMap(data => {
          return of(data);
    }));
  }

  public remove(id) {
    return this.http.delete(
      `${this.API}/${id}`).pipe(
        take(1)
      );
  }

}
