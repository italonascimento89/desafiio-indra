
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosComponent } from './../usuarios.component';
import { UsuariosRoutingMudule } from './historico-combustivel-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        UsuariosComponent,
    ],
    imports: [
        CommonModule,
        UsuariosRoutingMudule,
        ModalModule.forRoot(),
        SharedModule
    ]
})
export class UsuariosModule { }
