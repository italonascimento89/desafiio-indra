export interface UsuariosInterface {
    id: number;
    nome: string;
    email: string;
    usuario: string;
    senha: string;
}