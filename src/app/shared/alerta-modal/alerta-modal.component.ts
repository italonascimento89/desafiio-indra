import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-alerta-modal',
  templateUrl: './alerta-modal.component.html',
  styleUrls: ['./alerta-modal.component.css']
})
export class AlertaModalComponent implements OnInit {

  @Input() public  type = 'bg-primary';
  @Input() public  message: string;

  constructor(
     public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }


  public onClose() {
    this.bsModalRef.hide();
  }

}
