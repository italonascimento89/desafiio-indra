import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AlertaModalComponent } from './alerta-modal/alerta-modal.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { ErrorMsgComponent } from './error-msg/error-msg.component';


@NgModule({
  declarations:[
    AlertaModalComponent,
    InputFieldComponent,
    ErrorMsgComponent,
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    AlertaModalComponent,
    InputFieldComponent,
    ErrorMsgComponent,
  ],
  entryComponents: [AlertaModalComponent]
})
export class SharedModule { }


