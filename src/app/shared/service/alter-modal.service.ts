import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertaModalComponent } from '../alerta-modal/alerta-modal.component';

export enum AlertTypes {

  DANGER = 'danger',
  SUCESS = 'success'

}

@Injectable({
  providedIn: 'root'
})
export class AlterModalService {

  constructor(
    private modalService: BsModalService
  ) { }


  private showAlert(message: string, type: string, dimissTimeOut?: number) {
    const bsModalRef: BsModalRef = this.modalService.show(AlertaModalComponent);
    bsModalRef.content.type = type;
    bsModalRef.content.message = message;

    if (dimissTimeOut) {
      setTimeout (() => bsModalRef.hide(), dimissTimeOut);
    }

  }


  public showAlertDanger(message: string) {

   this.showAlert(message, AlertTypes.DANGER);

  }

  public showAlertSucess(message: string) {

    this.showAlert(message, AlertTypes.SUCESS, 2000);

   }

}
