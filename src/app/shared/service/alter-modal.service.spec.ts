import { TestBed } from '@angular/core/testing';

import { AlterModalService } from './alter-modal.service';

describe('AlterModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlterModalService = TestBed.get(AlterModalService);
    expect(service).toBeTruthy();
  });
});
