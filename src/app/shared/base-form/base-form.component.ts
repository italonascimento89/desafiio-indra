import { OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { CadastroUsuariosService } from 'src/app/cadastro-usuarios/service/cadastro-usuarios.service';
import { AlterModalService } from '../service/alter-modal.service';

export abstract class BaseFormComponent implements OnInit {

  public form: FormGroup;
  public submiited: boolean = false;

  constructor(
    public service: CadastroUsuariosService,
    public modal: AlterModalService,
  ) { }

  ngOnInit() {
  }

  abstract submit();

  public onSubmit() {
    if (this.form.valid) {
      this.submit();
    } else {
      console.log('formulario invalido');
      this.verificaValidacoesForm(this.form);
    }
  }

  public verificaValidacoesForm(formGroup: FormGroup | FormArray) {
    Object.keys(formGroup.controls).forEach(campo => {
      console.log(campo);
      const controle = formGroup.get(campo);
      controle.markAsDirty();
      controle.markAsTouched();
      if (controle instanceof FormGroup || controle instanceof FormArray) {
        this.verificaValidacoesForm(controle);
      }
    });
  }
/**
  public resetar() {
    this.form.reset();
  }
   */

  public verificaValidTouched(campo: string) {
    return (
      !this.form.get(campo).valid &&
      (this.form.get(campo).touched || this.form.get(campo).dirty)
    );
  }

  public verificaRequired(campo: string) {
    return (
      this.form.get(campo).hasError('required') &&
      (this.form.get(campo).touched || this.form.get(campo).dirty)
    );
  }

  aplicaCssErro(campo: string) {

    if (!this.verificaValidTouched(campo)) {
      return  'form-control'
    }
    return {
      'form-control is-valid': this.verificaValidTouched(campo),
      'form-control is-invalid': this.verificaValidTouched(campo)
    };
  }


}