import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoricoCombustivelComponent } from '../historico-combustivel.component';
import { HistoricoCombustivelRoutingMudule } from 'src/app/historico-combustivel/module/historico-combustivel-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { UsuariosService } from 'src/app/usuarios/service/usuarios-service.service';

@NgModule({
  declarations: [
    HistoricoCombustivelComponent
  ],
  imports: [
    CommonModule,
    HistoricoCombustivelRoutingMudule,
    HttpClientModule,
  ],
  providers: [
    UsuariosService
  ]
})
export class HistoricoCombustivelModule { }
