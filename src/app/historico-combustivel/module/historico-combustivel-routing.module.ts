import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoricoCombustivelComponent } from './../historico-combustivel.component';

const routes: Routes = [
  {
    path: '',
    component: HistoricoCombustivelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoricoCombustivelRoutingMudule { }