import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  public headerActive: boolean = true;

  constructor() { }

  
  public setHeaderActive(menu: string): void {
    console.log(menu)
    
    if (menu === 'cadastrar') {
      this.headerActive = true;
    }
    else {
      this.headerActive = false;
    }

    console.log(this.headerActive)
      
  }

  public getHeaderActive(): boolean {
    
    return this.headerActive;
    
  }
   

}
