import { Component, OnInit } from '@angular/core';

import { SidebarService } from './service/sidebar.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(
    private sidebarService: SidebarService
  ) { }

  ngOnInit() {
  }

  public setHeaderActive(): void {

    this.sidebarService.setHeaderActive('cadastrar');

  }



}
